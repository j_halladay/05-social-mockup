Assessment: Style a Social Network 2-Column Layout
For this assessment, you will continue prototyping a layout for a social networking site, this time focusing on the two-column layout as shown in the mockup below. You should continue developing within the '05-social-mockup' folder you used for the last activity, in which you created a 850 pixel wide header.

The left column should be 328 pixels wide (including borders), the right column should be 512 pixels wide (including borders), and the space separating them should be 10 pixels wide. This should exactly match the 850 pixel wide header you previously created.

You don't have to reproduce the entire layout below, however your prototype must show at least one appropriately sized box (with content) in both the left and the right column. Submit your Cloud9 environment url to complete this assessment.

